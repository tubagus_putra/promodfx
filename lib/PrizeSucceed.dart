import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:promodfx/components/AppNavBar.dart';

class PrizeSucceed extends StatefulWidget {
  const PrizeSucceed({ Key? key }) : super(key: key);

  @override
  _PrizeSucceedState createState() => _PrizeSucceedState();
}

class _PrizeSucceedState extends State<PrizeSucceed> {
  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: Colors.white,
    primary: const Color(0xff1F46A2),
    minimumSize: const Size(88, 36),
    padding: const EdgeInsets.symmetric(
      horizontal: 64.0,
      vertical: 20.0,
    ),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(28)
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          margin: const EdgeInsets.symmetric(
            horizontal: 32.0,
          ),
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Padding(
                padding: EdgeInsets.only(
                  bottom: 32.0,
                ),
                child: Text(
                  "Selamat!",
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.w900,
                    color: Color(0xff1F46A2),
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  bottom: 32.0,
                ),
                child: SvgPicture.asset(
                  'images/holding_rupiah.svg',
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  bottom: 32.0,
                ),
                child: RichText(
                  text: const TextSpan(
                    style: TextStyle(
                      fontSize: 18,
                      color: Color(0xff494848),
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: 'Rp 600.000\n',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        )
                      ),
                      TextSpan(text: 'akan segera ditransfer ke\nrekening bank Anda'),
                    ],
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Container(
                width: 280,
                margin: const EdgeInsets.only(
                  bottom: 40.0,
                ),
                decoration: BoxDecoration(
                  color: const Color(0xffF6F7FA),
                  borderRadius: BorderRadius.circular(12),
                ),
                clipBehavior: Clip.hardEdge,
                padding: const EdgeInsets.symmetric(
                  horizontal: 14.0,
                  vertical: 10.0,
                ),
                child: Row(
                  children: const <Widget>[
                    Padding(
                      padding: EdgeInsets.only(
                        right: 6.0,
                      ),
                      child: Icon(
                        Icons.access_time_outlined,
                        color: Color(0xff1F46A2),
                        size: 27,
                      ),
                    ),
                    Text(
                      'jadwal pembayaran pinjaman Anda\ndiperbarui',
                      style: TextStyle(
                        color: Color(0xff1F46A2),
                      ),
                    ),
                  ],
                ),
              ),
              ElevatedButton(
                style: raisedButtonStyle,
                onPressed: () {
                  Navigator.pushNamed(context, '/');
                },
                child: const Text(
                  'Lihat jadwal pembayaran',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: const AppNavBar(),
    );
  }
}