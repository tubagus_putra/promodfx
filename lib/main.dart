import 'package:flutter/material.dart';

import 'package:promodfx/ConfirmationPrize.dart';
import 'package:promodfx/GetAdditionalMoney.dart';
import 'package:promodfx/PrizeSucceed.dart';
import 'package:promodfx/AdditionalComp.dart';

void main() {
  runApp(const PromodfxApp());
}

class PromodfxApp extends StatelessWidget {
  const PromodfxApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => const AdditionalComp(),
        '/getmoney': (context) => const GetAdditionalMoney(),
        '/confirmation': (context) => const ConfirmationPrize(),
        '/succeed': (context) => const PrizeSucceed(),
      },
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
      ),
    );
  }
}
