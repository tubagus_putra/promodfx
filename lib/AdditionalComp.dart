import 'package:flutter/material.dart';

import 'package:promodfx/components/ActiveLoanBanner.dart';
import 'package:promodfx/components/TopupExtraBanner.dart';

class AdditionalComp extends StatelessWidget {
  const AdditionalComp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.symmetric(
              horizontal: 32.0,
            ),
            child: Column(
              children: const <Widget>[
                SizedBox(
                  height: 32.0,
                ),
                Center(
                  child: Text(
                  'Components',
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                      color: Color(0xff1F46A2),
                    ),
                  ),
                ),
                SizedBox(
                  height: 24.0,
                ),
                Padding(
                  padding: EdgeInsets.only(
                    bottom: 8.0,
                  ),
                  child: Text(
                    'Active Loan Banner',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                ActiveLoanBanner(),
                Padding(
                  padding: EdgeInsets.only(
                    bottom: 8.0,
                  ),
                  child: Text(
                    'Topup Extra Banner',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
                TopupExtraBanner(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}