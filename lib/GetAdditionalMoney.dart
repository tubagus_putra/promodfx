import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:promodfx/components/AppNavBar.dart';

class GetAdditionalMoney extends StatefulWidget {
  const GetAdditionalMoney({ Key? key }) : super(key: key);

  @override
  _GetAdditionalMoneyState createState() => _GetAdditionalMoneyState();
}

class _GetAdditionalMoneyState extends State<GetAdditionalMoney> {
  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: Colors.white,
    primary: const Color(0xff1F46A2),
    minimumSize: const Size(88, 36),
    padding: const EdgeInsets.symmetric(
      horizontal: 64.0,
      vertical: 20.0,
    ),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(28)
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          margin: const EdgeInsets.symmetric(
            horizontal: 32.0,
          ),
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              const Padding(
                padding: EdgeInsets.only(
                  bottom: 24.0,
                ),
                child: Text(
                  "Dapatkan ekstra\nRp 600.000 Sekarang",
                  style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.w900,
                    color: Color(0xff1F46A2),
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  bottom: 32.0,
                ),
                child: SvgPicture.asset(
                  'images/people_rupiah.svg',
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  bottom: 32.0,
                ),
                child: RichText(
                  text: const TextSpan(
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xff494848),
                    ),
                    children: <TextSpan>[
                      TextSpan(text: 'Butuh uang tambahan?\n\n'),
                      TextSpan(text: 'Danafix dapat menambahkan\n'),
                      TextSpan(
                        text: 'Rp 600.000',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        )
                      ),
                      TextSpan(text: ' ke rekening bank Anda.'),
                    ],
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              ElevatedButton(
                style: raisedButtonStyle,
                onPressed: () {
                  Navigator.pushNamed(context, '/confirmation');
                },
                child: const Text(
                  'Dapatkan uang tambahan',
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: const AppNavBar(),
    );
  }
}
