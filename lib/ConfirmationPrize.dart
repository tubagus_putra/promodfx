import 'package:flutter/material.dart';

import 'package:promodfx/components/AppNavBar.dart';

class ConfirmationPrize extends StatefulWidget {
  const ConfirmationPrize({ Key? key }) : super(key: key);

  @override
  _ConfirmationPrizeState createState() => _ConfirmationPrizeState();
}

class _ConfirmationPrizeState extends State<ConfirmationPrize> {
  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: Colors.white,
    primary: const Color(0xff1F46A2),
    minimumSize: const Size(88, 36),
    padding: const EdgeInsets.symmetric(
      horizontal: 64.0,
      vertical: 20.0,
    ),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(28)
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            margin: const EdgeInsets.symmetric(
              horizontal: 32.0,
            ),
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.only(
                    top: 36.0,
                    bottom: 24.0,
                  ),
                  child: Text(
                    "Dapatkan ekstra\nRp 600.000 Sekarang",
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w900,
                      color: Color(0xff1F46A2),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    bottom: 28.0,
                  ),
                  child: RichText(
                    text: const TextSpan(
                      style: TextStyle(
                        fontSize: 14,
                        color: Color(0xff494848),
                      ),
                      children: <TextSpan>[
                        TextSpan(text: 'Anda akan mendapatkan '),
                        TextSpan(
                          text: 'Rp 600.000\n',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          )
                        ),
                        TextSpan(text: 'Parameter pinjaman Anda akan berubah'),
                      ],
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    bottom: 24.0,
                  ),
                  child: Container(
                    clipBehavior: Clip.hardEdge,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black.withOpacity(0.2),
                          spreadRadius: 0,
                          blurRadius: 2,
                          offset: const Offset(0, 2),
                        ),
                      ],
                    ),
                    child: Table(
                      children: [
                        TableRow(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(16.0),
                              color: const Color(0xffF6F7FA),
                              child: const Text(
                                '',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff827E7E),
                                  fontSize: 12.48,
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.all(16.0),
                              color: const Color(0xffF6F7FA),
                              child: const Text(
                                'Sebelum',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff827E7E),
                                  fontSize: 12.48,
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.all(16.0),
                              color: const Color(0xffF6F7FA),
                              child: const Text(
                                'Setelah',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff827E7E),
                                  fontSize: 12.48,
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 16.0,
                                vertical: 12.0,
                              ),
                              child: const Text(
                                'Pembayaran\nper bulan',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff827E7E),
                                  fontSize: 10,
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 16.0,
                                vertical: 12.0,
                              ),
                              child: const Text(
                                'Rp 545.000',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Color(0xff827E7E),
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 16.0,
                                vertical: 12.0,
                              ),
                              child: const Text(
                                'Rp 698.000',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Color(0xff827E7E),
                                ),
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 16.0,
                                vertical: 12.0,
                              ),
                              child: const Text(
                                'Jangka waktu',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff827E7E),
                                  fontSize: 10.0,
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 16.0,
                                vertical: 12.0,
                              ),
                              child: const Text(
                                '4 bulan',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 12.0,
                                  color: Color(0xff827E7E),
                                ),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 16.0,
                                vertical: 12.0,
                              ),
                              child: const Text(
                                '5 bulan',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Color(0xff827E7E),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    bottom: 48.0,
                  ),
                  child: Column(
                    children: const <Widget>[
                      ListTile(
                        dense: true,
                        minLeadingWidth: 0,
                        minVerticalPadding: 0,
                        visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                        leading: Icon(
                          Icons.fiber_manual_record,
                          size: 10,
                          color: Color(0xff1F46A2),
                        ),
                        title: Text(
                          'Anda akan mendapatkan Rp 600.000. sekarang',
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      ),
                      ListTile(
                        dense: true,
                        minLeadingWidth: 0,
                        minVerticalPadding: 0,
                        visualDensity: VisualDensity(horizontal: 0, vertical: -4),
                        leading: Icon(
                          Icons.fiber_manual_record,
                          size: 10,
                          color: Color(0xff1F46A2),
                        ),
                        title: Text(
                          'Pinjaman aktif Anda pembayaran bulanan baru akan menjadi Rp 698.000',
                          style: TextStyle(
                            fontSize: 12,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                ElevatedButton(
                  style: raisedButtonStyle,
                  onPressed: () {
                    Navigator.pushNamed(context, '/succeed');
                  },
                  child: const Text(
                    'Konfirmasi dan dapatkan uang',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      bottomNavigationBar: const AppNavBar(),
    );
  }
}