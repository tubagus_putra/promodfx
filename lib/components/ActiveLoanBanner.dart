import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:promodfx/components/DialogPromo.dart';

class ActiveLoanBanner extends StatefulWidget {
  const ActiveLoanBanner({ Key? key }) : super(key: key);

  @override
  _ActiveLoanBannerState createState() => _ActiveLoanBannerState();
}

class _ActiveLoanBannerState extends State<ActiveLoanBanner> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(
        bottom: 24.0,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        border: Border.all(
          color: const Color(0xffE1E1E1),
        ),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            spreadRadius: 0,
            blurRadius: 16,
            offset: const Offset(0, 6),
          ),
        ],
      ),
      clipBehavior: Clip.hardEdge,
      padding: const EdgeInsets.all(24.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Column>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Padding(
                padding: EdgeInsets.only(
                  bottom: 6.0,
                ),
                child: Text(
                  'Dapatkan ekstra Rp 600.000\nSekarang',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w900,
                    color: Color(0xff1F46A2),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  bottom: 6.0,
                ),
                child: RichText(
                  text: const TextSpan(
                    style: TextStyle(
                      fontSize: 12,
                      color: Color(0xff494848),
                    ),
                    children: <TextSpan>[
                      TextSpan(text: 'Anda bisa mendapatkan tambahan'),
                      TextSpan(
                        text: '\nRp 600.000',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        )
                      ),
                      TextSpan(text: ' ke rekening bank Anda\nhari ini.'),
                    ],
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  onPrimary: Colors.white,
                  primary: const Color(0xff1F46A2),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 64.0,
                    vertical: 12.0,
                  ),
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(28)
                    ),
                  ),
                ),
                onPressed: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) => const DialogPromo(),
                  );
                },
                child: const Text(
                  'Lihat Detail',
                  style: TextStyle(
                    fontSize: 10,
                  ),
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SvgPicture.asset(
                'images/prize_wallet.svg',
                width: 78.0,
              ),
            ],
          ),
        ],
      ),
    );
  }
}