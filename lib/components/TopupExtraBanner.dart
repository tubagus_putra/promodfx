import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TopupExtraBanner extends StatefulWidget {
  const TopupExtraBanner({ Key? key }) : super(key: key);

  @override
  _TopupExtraBannerState createState() => _TopupExtraBannerState();
}

class _TopupExtraBannerState extends State<TopupExtraBanner> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 260,
      margin: const EdgeInsets.only(
        bottom: 40.0,
      ),
      decoration: BoxDecoration(
        color: const Color(0xffF6F7FA),
        borderRadius: BorderRadius.circular(12),
      ),
      clipBehavior: Clip.hardEdge,
      padding: const EdgeInsets.symmetric(
        horizontal: 16.0,
        vertical: 16.0,
      ),
      child: Row(
        verticalDirection: VerticalDirection.down,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
              right: 12.0,
            ),
            child: SvgPicture.asset(
              'images/rupiah_currency.svg',
            ),
          ),
          RichText(
            text: const TextSpan(
              style: TextStyle(
                fontSize: 14,
                color: Color(0xff1F46A2),
              ),
              children: <TextSpan>[
                TextSpan(text: 'Top up extra cash '),
                TextSpan(
                  text: 'Rp 600.000',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  )
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}