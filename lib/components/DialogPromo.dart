import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DialogPromo extends StatefulWidget {
  const DialogPromo({ Key? key }) : super(key: key);

  @override
  _DialogPromoState createState() => _DialogPromoState();
}

class _DialogPromoState extends State<DialogPromo> {
  final ButtonStyle raisedButtonStyle = ElevatedButton.styleFrom(
    onPrimary: Colors.white,
    primary: const Color(0xff1F46A2),
    minimumSize: const Size.fromHeight(40),
    padding: const EdgeInsets.symmetric(
      horizontal: 64.0,
      vertical: 15.0,
    ),
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(
        Radius.circular(28)
      ),
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(18.0),
      ),
      elevation: 16.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  Widget dialogContent(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        left: 0.0,
        right: 0.0,
      ),
      child: Stack(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(12.0),
            margin: const EdgeInsets.only(
              top: 13.0,
              right: 8.0,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(18.0),
              boxShadow: const <BoxShadow>[
                BoxShadow(
                  color: Colors.black26,
                  blurRadius: 0.0,
                  offset: Offset(0.0, 0.0),
                ),
              ]
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.only(
                    bottom: 20.0,
                    top: 20.0,
                  ),
                  child: Text(
                    'Dapatkan ekstra\nRp 600.000 Sekarang',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w700,
                      color: Color(0xff1F46A2),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    bottom: 20.0,
                  ),
                  child: SvgPicture.asset(
                    'images/coin_rupiah.svg',
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(
                    bottom: 12.0,
                  ),
                  child: Text(
                    'Hai Heru',
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w700,
                      color: Color(0xff1F46A2),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    bottom: 32.0,
                  ),
                  child: RichText(
                    text: const TextSpan(
                      style: TextStyle(
                        fontSize: 16,
                        color: Color(0xff494848),
                      ),
                      children: <TextSpan>[
                        TextSpan(text: 'Anda bisa mendapatkan tambahan'),
                        TextSpan(
                          text: '\nRp 600.000',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          )
                        ),
                        TextSpan(text: ' ke rekening bank Anda\nhari ini.'),
                      ],
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    bottom: 16.0,  
                  ),
                  child: ElevatedButton(
                    style: raisedButtonStyle,
                    onPressed: () {
                      Navigator.pushNamed(context, '/getmoney');
                    },
                    child: const Text(
                      'Lihat Detail',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            right: 0.0,
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: const Align(
                alignment: Alignment.topRight,
                child: CircleAvatar(
                  radius: 24.0,
                  backgroundColor: Color(0xff1F46A2),
                  child: Icon(
                    Icons.close,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
